import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:share/share.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

void main() {
  // Set `enableInDevMode` to true to see reports while in debug mode
  // This is only to be used for confirming that reports are being
  // submitted as expected. It is not intended to be used for everyday
  // development.
  Crashlytics.instance.enableInDevMode = true;

  // Pass all uncaught errors from the framework to Crashlytics.
  FlutterError.onError = Crashlytics.instance.recordFlutterError;

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.red,
          canvasColor: Colors.black87,
          scaffoldBackgroundColor: const Color(0xFF000000)),
      home: MyHomePage(title: 'Coronavirus  LIVE'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Completer<WebViewController> _controller = Completer<WebViewController>();

  Future<bool> _onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            backgroundColor: Colors.black,
            title: new Text('Are you sure?',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white)),
            content: new Text('Do you want to exit the App',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white)),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () {
                  SystemNavigator.pop();
                },
                child: new Text('Yea'),
              ),
            ],
          ),
        )) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
//          centerTitle: true,
            title: Text(widget.title),
            leading: Builder(
                builder: (context) => IconButton(
                    icon: Icon(Icons.settings),
                    onPressed: () => Scaffold.of(context).openDrawer()))),
        drawer: Drawer(
          child: DrawerContent(),
        ),
        body: Center(
          child: WebView(
            initialUrl:
                'https://gisanddata.maps.arcgis.com/apps/opsdashboard/index.html#/85320e2ea5424dfaaa75ae62e5c06e61',
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);
            },
            javascriptMode: JavascriptMode.unrestricted,
          ),
        ),
      ),
    );
  }
}

class DrawerContent extends StatelessWidget {
  const DrawerContent({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        SizedBox(height: 100.0),
        /*DrawerHeader(
          decoration: BoxDecoration(
//                color: Colors.black87,
              ),
          child: Text(
            '',
            style: TextStyle(
              color: Colors.white,
              fontSize: 24,
            ),
          ),
        ),*/
        ListTile(
          leading: Icon(Icons.share, color: Colors.white70),
          title: Text('Share', style: TextStyle(color: Colors.white)),
          onTap: () {
            Share.share(
                'check out my new app at https://play.google.com/store/apps/details?id=com.nightmareinc.truth_or_dare',
                subject: 'Coronovirus');
          },
        ),
        ListTile(
          leading: Icon(Icons.android, color: Colors.white70),
          title: Text('Game Version: 1.0.0',
              style: TextStyle(color: Colors.white)),
        ),
        ListTile(
          leading: Icon(Icons.email, color: Colors.white70),
          title: Text('Contact Us', style: TextStyle(color: Colors.white)),
          onTap: () {
            showDialog(
              context: context,
              builder: (_) => AlertDialog(
                title: Text('Feel free to reach us by this email:'),
                content: Text('nazanin.dev@gmail.com'),
                actions: [
                  FlatButton(
                    child: Text('Cool'),
                    onPressed: () {
//                          dispose();
                    },
                  ),
                ],
                elevation: 24.0,
                backgroundColor: Colors.grey,
              ),
            );
            /*AlertDialog(
              title: Text('Feel free to reach us by this email:'),
              content: Text('nazanin.dev@gmail.com'),
              actions: [
                FlatButton(child: Text('Cool'),
                onPressed: () {
                  return true
                },),
              ],
              elevation: 24.0,
              backgroundColor: Colors.grey,
            );*/
            /*showDialog(
                context: context,
                builder: (_) => WillPopScope(
                      onWillPop: () {},
                      child: Dialog(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child:
                              Text('naz', style: TextStyle(fontSize: 20.0)),
                        ),
                        backgroundColor: Colors.grey,
                        elevation: 15.0,
                      ),
                    ));*/
          },
        ),
      ],
    );
  }
}
